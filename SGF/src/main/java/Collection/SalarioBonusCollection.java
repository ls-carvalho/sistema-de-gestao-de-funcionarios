/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Collection;

import Model.SalarioBonusModel;
import Presenter.Observer.SalarioBonusSubject;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucas Carvalho
 */
public class SalarioBonusCollection extends SalarioBonusSubject {

    private static SalarioBonusCollection dadosCollection;
    private List<SalarioBonusModel> salarioBonusCollection = new ArrayList();

    public SalarioBonusCollection() {
    }

    public static SalarioBonusCollection getInstancia() {
        if (dadosCollection == null) {
            dadosCollection = new SalarioBonusCollection();
        }
        return dadosCollection;
    }

    public List<SalarioBonusModel> getSalarioBonusCollection() {
        return salarioBonusCollection;
    }

    public void setSalarioBonusCollection(List<SalarioBonusModel> salarioBonusCollection) {
        this.salarioBonusCollection = salarioBonusCollection;
        notifyObserver();
    }

    public List<SalarioBonusModel> getSalarioCollectionPorData(LocalDate dataBusca) {
        List<SalarioBonusModel> buscaRefinada = new ArrayList();
        for (var salario : salarioBonusCollection) {
            if (salario.getData().equals(dataBusca)) {
                buscaRefinada.add(salario);
            }
        }
        return buscaRefinada;
    }

    public void addSalarioBonus(SalarioBonusModel salarioBonusNovo) {
        this.salarioBonusCollection.add(salarioBonusNovo);
        notifyObserver();
    }
    
    public List<SalarioBonusModel> getSalarioCollectionPorFuncionarioID(int id){
        List<SalarioBonusModel> buscaRefinada = new ArrayList();
        for (var salario : salarioBonusCollection) {
            if (salario.getId() == id) {
                buscaRefinada.add(salario);
            }
        }
        return buscaRefinada;
    }
}
