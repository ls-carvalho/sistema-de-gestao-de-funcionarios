/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Collection;

import Model.FuncionarioModel;
import Presenter.Observer.FuncionarioSubject;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucas Carvalho
 */
public class FuncionarioCollection extends FuncionarioSubject {

    private static FuncionarioCollection dadosCollection;
    private List<FuncionarioModel> funcionarioCollection = new ArrayList();

    private FuncionarioCollection() {
    }

    public static FuncionarioCollection getInstancia() {
        if (dadosCollection == null) {
            dadosCollection = new FuncionarioCollection();
        }
        return dadosCollection;
    }

    public List<FuncionarioModel> getFuncionarioCollection() {
        return funcionarioCollection;
    }

    public void setFuncionarioCollection(List<FuncionarioModel> funcionarioCollection) {
        this.funcionarioCollection = funcionarioCollection;
        notifyObserver();
    }

    public int getNextIdFuncionario() {
        if (this.funcionarioCollection.isEmpty()) {
            return 1;
        }
        int id = 0;
        while (id < this.funcionarioCollection.size()) {
            id++;
        }
        return id + 1;
    }

    public FuncionarioModel getFuncionarioModelPorID(int id) {
        for (var func : this.funcionarioCollection) {
            if (func.getId() == id) {
                return func;
            }
        }
        return null;
    }

    public void setFuncionarioModelPorID(FuncionarioModel novoFuncionario, int id) {
        for (var func : this.funcionarioCollection) {
            if (func.getId() == id) {
                func = novoFuncionario;
                notifyObserver();
                return;
            }
        }
    }

    public void removeFuncionarioModelPorID(int id) {
        for (var func : this.funcionarioCollection) {
            if (func.getId() == id) {
                this.funcionarioCollection.remove(func);
                notifyObserver();
                return;
            }
        }
    }

    public void addFuncionario(FuncionarioModel novoFuncionario) {
        this.funcionarioCollection.add(novoFuncionario);
        notifyObserver();
    }

    public int getNumeroFuncionarioCadastrado() {
        return this.funcionarioCollection.size();
    }

    public List<FuncionarioModel> getBuscaRefinada(String busca) {
        List<FuncionarioModel> buscaRefinada = new ArrayList();
        for (var func : this.funcionarioCollection) {
            if (func.getNome().contains(busca)) {
                buscaRefinada.add(func);
            }
        }
        return buscaRefinada;
    }
}
