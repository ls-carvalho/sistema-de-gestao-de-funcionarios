/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View.ManterFuncionario;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Lucas Carvalho
 */
public class BuscaFuncionarioView extends javax.swing.JInternalFrame {

    /**
     * Creates new form BuscaView
     */
    public BuscaFuncionarioView() {
        initComponents();
    }

    public JButton getBotaoBuscarFuncionarioFiltro() {
        return botaoBuscarFuncionarioFiltro;
    }

    public JButton getBotaoFechar() {
        return botaoFechar;
    }

    public JButton getBotaoNovoFuncionario() {
        return botaoNovoFuncionario;
    }

    public JButton getBotaoVerBonusFuncionario() {
        return botaoVerBonusFuncionario;
    }

    public JButton getBotaoVisualizarFuncionario() {
        return botaoVisualizarFuncionario;
    }

    public JTable getTabelaFuncionario() {
        return tabelaFuncionario;
    }

    public JTextField getTextoFuncionarioFiltro() {
        return textoFuncionarioFiltro;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        textoFuncionarioFiltro = new javax.swing.JTextField();
        botaoBuscarFuncionarioFiltro = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaFuncionario = new javax.swing.JTable();
        botaoFechar = new javax.swing.JButton();
        botaoVisualizarFuncionario = new javax.swing.JButton();
        botaoVerBonusFuncionario = new javax.swing.JButton();
        botaoNovoFuncionario = new javax.swing.JButton();

        setClosable(true);

        jLabel1.setText("Nome");

        botaoBuscarFuncionarioFiltro.setText("Buscar");

        tabelaFuncionario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nome", "Idade", "Cargo", "Bonus", "Salário"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelaFuncionario.setColumnSelectionAllowed(true);
        tabelaFuncionario.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tabelaFuncionario);
        tabelaFuncionario.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        botaoFechar.setText("Fechar");

        botaoVisualizarFuncionario.setText("Visualizar");

        botaoVerBonusFuncionario.setText("Ver Bônus");

        botaoNovoFuncionario.setText("Novo");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 688, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(textoFuncionarioFiltro)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botaoBuscarFuncionarioFiltro))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(botaoFechar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botaoNovoFuncionario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botaoVerBonusFuncionario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botaoVisualizarFuncionario)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textoFuncionarioFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botaoBuscarFuncionarioFiltro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoFechar)
                    .addComponent(botaoVisualizarFuncionario)
                    .addComponent(botaoVerBonusFuncionario)
                    .addComponent(botaoNovoFuncionario))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoBuscarFuncionarioFiltro;
    private javax.swing.JButton botaoFechar;
    private javax.swing.JButton botaoNovoFuncionario;
    private javax.swing.JButton botaoVerBonusFuncionario;
    private javax.swing.JButton botaoVisualizarFuncionario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaFuncionario;
    private javax.swing.JTextField textoFuncionarioFiltro;
    // End of variables declaration//GEN-END:variables
}
