/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucas Carvalho
 */
public class SalarioBonusModel {

    private int id;
    private LocalDate data;
    private double salarioBase;
    private boolean bonusGeneroso;
    private List<BonusModel> bonus = new ArrayList();
    private double salarioFinal;

    public SalarioBonusModel(int id, LocalDate data, double salarioBase, boolean bonusGeneroso) {
        this.id = id;
        this.data = data;
        this.salarioBase = salarioBase;
        this.salarioFinal = this.salarioBase;
        this.bonusGeneroso = bonusGeneroso;
    }

    public void addBonus(BonusModel bonus) {
        this.bonus.add(bonus);
        this.salarioFinal = this.salarioBase + getValorBonusTotal();
    }

    public boolean isBonusGeneroso() {
        return bonusGeneroso;
    }

    public void setBonusGeneroso(boolean bonusGeneroso) {
        this.bonusGeneroso = bonusGeneroso;
    }

    public List<BonusModel> getBonus() {
        return bonus;
    }

    public void setBonus(List<BonusModel> bonus) {
        this.bonus = bonus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public double getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(double salarioBase) {
        this.salarioBase = salarioBase;
    }

    public double getSalarioFinal() {
        return salarioFinal;
    }

    public double getValorBonusTotal() {
        double aux = 0;
        for (BonusModel b : bonus) {
            aux = b.getValor() + aux;
        }
        return aux;
    }
}
