/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.LocalDate;

/**
 *
 * @author Lucas Carvalho
 */
public class FuncionarioModel {

    private String nome;
    private String cargo;
    private int idade;
    private String bonus;
    private double salario;
    private int falta;
    private LocalDate admissao;
    private boolean funcionarioDoMes;
    private int id;

    public FuncionarioModel(String nome, String cargo, int idade, String bonus, double salario, int falta, LocalDate admissao, boolean funcionarioDoMes, int id) {
        this.nome = nome;
        this.cargo = cargo;
        this.idade = idade;
        this.bonus = bonus;
        this.salario = salario;
        this.falta = falta;
        this.admissao = admissao;
        this.funcionarioDoMes = funcionarioDoMes;
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public int getFalta() {
        return falta;
    }

    public void setFalta(int falta) {
        this.falta = falta;
    }

    public LocalDate getAdmissao() {
        return admissao;
    }

    public void setAdmissao(LocalDate admissao) {
        this.admissao = admissao;
    }

    public boolean isFuncionarioDoMes() {
        return funcionarioDoMes;
    }

    public void setFuncionarioDoMes(boolean funcionarioDoMes) {
        this.funcionarioDoMes = funcionarioDoMes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
