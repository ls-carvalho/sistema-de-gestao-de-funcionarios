/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.ChainOfResponsibility;

import Collection.FuncionarioCollection;
import Model.SalarioBonusModel;

/**
 *
 * @author Lucas Carvalho
 */
public class CalculaBonusCargoCOR extends CalculaBonusCOR {

    public CalculaBonusCargoCOR(String nome) {
        super(nome);
    }

    @Override
    public boolean aceitar(SalarioBonusModel salarioBonusModel) {
        return !(FuncionarioCollection.getInstancia().getFuncionarioModelPorID(salarioBonusModel.getId()).getCargo().equals("Estagiário"));
    }

    @Override
    public void calcular(SalarioBonusModel salarioBonusModel) {
        var funcionarioModel = FuncionarioCollection.getInstancia().getFuncionarioModelPorID(salarioBonusModel.getId());
        var cargo = funcionarioModel.getCargo();
        double generoso = 1;
        if(funcionarioModel.getBonus().equals("Generoso")){
            generoso = 2;
        }
        if (cargo.equals("Pleno")) {
            var valor = funcionarioModel.getSalario() * 0.05 * generoso;
            setValor(valor);
        } else if (cargo.equals("Gerente")) {
            var valor = funcionarioModel.getSalario() * 0.1 * generoso;
            setValor(valor);
        }
        super.adicionaBonus(salarioBonusModel, this);
    }
}
