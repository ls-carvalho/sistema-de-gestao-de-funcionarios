/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.ChainOfResponsibility;

import Collection.FuncionarioCollection;
import Model.SalarioBonusModel;

/**
 *
 * @author Lucas Carvalho
 */
public class CalculaBonusAssiduidadeCOR extends CalculaBonusCOR {

    public CalculaBonusAssiduidadeCOR(String nome) {
        super(nome);
    }

    @Override
    public boolean aceitar(SalarioBonusModel salarioBonusModel) {        
        return FuncionarioCollection.getInstancia().getFuncionarioModelPorID(salarioBonusModel.getId()).getFalta() < 6;
    }

    @Override
    public void calcular(SalarioBonusModel salarioBonusModel) {
        var funcionarioModel = FuncionarioCollection.getInstancia().getFuncionarioModelPorID(salarioBonusModel.getId());
        var nroFaltas = funcionarioModel.getFalta();
        double generoso = 1;
        if(funcionarioModel.getBonus().equals("Generoso")){
            generoso = 2;
        }
        if (nroFaltas == 0) {
            var valor = funcionarioModel.getSalario() * 0.1 * generoso;
            setValor(valor);
        } else if (nroFaltas >= 1 && nroFaltas <= 3) {
            var valor = funcionarioModel.getSalario() * 0.05 * generoso;
            setValor(valor);
        } else if (nroFaltas >= 4 && nroFaltas <= 5) {
            var valor = funcionarioModel.getSalario() * 0.01 * generoso;
            setValor(valor);
        }
        super.adicionaBonus(salarioBonusModel, this);
    }
}
