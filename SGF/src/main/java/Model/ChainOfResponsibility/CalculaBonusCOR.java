/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.ChainOfResponsibility;

import Model.BonusModel;
import Model.SalarioBonusModel;

/**
 *
 * @author Lucas Carvalho
 */
public abstract class CalculaBonusCOR {

    private String nome;
    private double valor;

    public CalculaBonusCOR(String nome) {
        this.nome = nome;
    }

    public abstract boolean aceitar(SalarioBonusModel salarioBonusModel);

    public abstract void calcular(SalarioBonusModel salarioBonusModel);

    public void adicionaBonus(SalarioBonusModel salarioBonusModel, CalculaBonusCOR calculoBonusCOR) {
        salarioBonusModel.addBonus(new BonusModel(this.nome, this.valor));
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    
}
