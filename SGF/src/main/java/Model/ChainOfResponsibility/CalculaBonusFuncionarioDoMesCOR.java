/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.ChainOfResponsibility;

import Collection.FuncionarioCollection;
import Model.SalarioBonusModel;

/**
 *
 * @author Lucas Carvalho
 */
public class CalculaBonusFuncionarioDoMesCOR extends CalculaBonusCOR {

    public CalculaBonusFuncionarioDoMesCOR(String nome) {
        super(nome);
    }

    @Override
    public boolean aceitar(SalarioBonusModel salarioBonusModel) {
        return FuncionarioCollection.getInstancia().getFuncionarioModelPorID(salarioBonusModel.getId()).isFuncionarioDoMes();
    }

    @Override
    public void calcular(SalarioBonusModel salarioBonusModel) {
        var funcionarioModel = FuncionarioCollection.getInstancia().getFuncionarioModelPorID(salarioBonusModel.getId());
        var nroFaltas = funcionarioModel.isFuncionarioDoMes();
        double generoso = 1;
        if(funcionarioModel.getBonus().equals("Generoso")){
            generoso = 2;
        }
        if (nroFaltas) {
            var valor = funcionarioModel.getSalario() * 0.2 * generoso;
            setValor(valor);
        }
        super.adicionaBonus(salarioBonusModel, this);
    }
}
