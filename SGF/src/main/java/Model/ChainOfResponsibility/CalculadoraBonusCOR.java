/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.ChainOfResponsibility;

import Model.SalarioBonusModel;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucas Carvalho
 */
public class CalculadoraBonusCOR {

    private List<CalculaBonusCOR> tratadores = new ArrayList();

    public CalculadoraBonusCOR() {
        tratadores.add(new CalculaBonusAssiduidadeCOR("Assiduidade"));
        tratadores.add(new CalculaBonusTempoDeServicoCOR("Tempo de Serviço"));
        tratadores.add(new CalculaBonusCargoCOR("Cargo"));
        tratadores.add(new CalculaBonusFuncionarioDoMesCOR("Funcionario do Mês"));
    }

    public void calculadora(SalarioBonusModel salarioBonusModel) {
        for (var tratador : tratadores) {
            if (tratador.aceitar(salarioBonusModel)) {
                tratador.calcular(salarioBonusModel);
            }
        }
    }
}
