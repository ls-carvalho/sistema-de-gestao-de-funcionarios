/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.ChainOfResponsibility;

import Collection.FuncionarioCollection;
import Model.SalarioBonusModel;

/**
 *
 * @author Lucas Carvalho
 */
public class CalculaBonusTempoDeServicoCOR extends CalculaBonusCOR {

    public CalculaBonusTempoDeServicoCOR(String nome) {
        super(nome);
    }

    @Override
    public boolean aceitar(SalarioBonusModel salarioBonusModel) {   
        var funcionario = FuncionarioCollection.getInstancia().getFuncionarioModelPorID(salarioBonusModel.getId());
        return funcionario.getAdmissao().isBefore(salarioBonusModel.getData().minusYears(1));
    }

    @Override
    public void calcular(SalarioBonusModel salarioBonusModel) {
        var funcionarioModel = FuncionarioCollection.getInstancia().getFuncionarioModelPorID(salarioBonusModel.getId());
        var dataAdmissao = funcionarioModel.getAdmissao();
        double generoso = 1;
        if(funcionarioModel.getBonus().equals("Generoso")){
            generoso = 2;
        }
        if(funcionarioModel.getAdmissao().isAfter(salarioBonusModel.getData().minusYears(5))){
            //1~5
            var valor = funcionarioModel.getSalario() * 0.02 * generoso;
            setValor(valor);
        } else if(funcionarioModel.getAdmissao().isAfter(salarioBonusModel.getData().minusYears(10))){
            //6~10 
            var valor = funcionarioModel.getSalario() * 0.03 * generoso;
            setValor(valor);
        } else if(funcionarioModel.getAdmissao().isAfter(salarioBonusModel.getData().minusYears(15))){
            //11=15
            var valor = funcionarioModel.getSalario() * 0.08 * generoso;
            setValor(valor);
        } else if(funcionarioModel.getAdmissao().isAfter(salarioBonusModel.getData().minusYears(20))){
            //16~20
            var valor = funcionarioModel.getSalario() * 0.1 * generoso;
            setValor(valor);
        } else {
            //>20
            var valor = funcionarioModel.getSalario() * 0.15 * generoso;
            setValor(valor);
        }
        super.adicionaBonus(salarioBonusModel, this);
    }
}
