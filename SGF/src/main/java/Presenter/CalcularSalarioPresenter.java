/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Collection.FuncionarioCollection;
import Collection.SalarioBonusCollection;
import Model.ChainOfResponsibility.CalculadoraBonusCOR;
import Model.FuncionarioModel;
import Model.SalarioBonusModel;
import Presenter.Observer.SalarioBonusObserver;
import View.ManterSalario.CalcularSalarioView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Lucas Carvalho
 */
public class CalcularSalarioPresenter implements SalarioBonusObserver {

    private CalcularSalarioView calcularSalarioView;

    public CalcularSalarioPresenter(JDesktopPane desktop) {
        this.calcularSalarioView = new CalcularSalarioView();
        desktop.add(this.calcularSalarioView);
        this.calcularSalarioView.setVisible(true);
        SalarioBonusCollection.getInstancia().add(this);
        listarTodosBonusSalario();
        this.calcularSalarioView.getBotaoLimparBusca().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA LISTAR TODOS OS ELEMENTOS DA LISTA
                listarTodosBonusSalario();
            }
        });
        this.calcularSalarioView.getBotaoBusca().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA LISTAR OS ELEMENTOS DE ACORDO COM O FILTRO
                refinarBusca(calcularSalarioView.getDataBusca().getText().toString());
            }
        });
        this.calcularSalarioView.getBotaoCalcularSalario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA REALIZAR OS CALCULOS DE SALÁRIO DE TODOS OS FUNCIONÁRIOS NA DATA ESPECIFICADA
                calcularSalario(calcularSalarioView.getDataCalculoSalario().getText());
            }
        });
        this.calcularSalarioView.getBotaoFechar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA FECHAR A JANELA
                calcularSalarioView.dispose();
            }
        });
    }

    public void listarTodosBonusSalario() {
        exibeTabela(SalarioBonusCollection.getInstancia().getSalarioBonusCollection());
    }

    public void refinarBusca(String busca) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate data = LocalDate.parse(busca, formatter);
        //Realiza a busca na Collection com a refinação
        List<SalarioBonusModel> listaBusca = SalarioBonusCollection.getInstancia().getSalarioCollectionPorData(data);
        //exibir a busca refinada na tela
        this.exibeTabela(listaBusca);
    }

    public void exibeTabela(List<SalarioBonusModel> listaSalario) {
        //Pega a referencia da tabela na view
        DefaultTableModel model = (DefaultTableModel) this.calcularSalarioView.getTabelaSalarios().getModel();
        //limpa a tabela
        model.setRowCount(0);
        //preenche a tabela
        for (var salario : listaSalario) {
            String[] dados = {String.valueOf(FuncionarioCollection.getInstancia().getFuncionarioModelPorID(salario.getId()).getNome()), salario.getData().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), String.valueOf(salario.getSalarioBase()), String.valueOf(salario.getValorBonusTotal()), String.valueOf(salario.getSalarioFinal())};
            model.addRow(dados);
        }
        
    }

    public void calcularSalario(String dataCalculo) {
        //pego a lista de funcionario
        try {
            if(dataCalculo.isBlank()){
                throw new Exception("Não se pode fazer um calculo sem data!");
            }
            List<String> nomes = new ArrayList();
            for (var func : FuncionarioCollection.getInstancia().getFuncionarioCollection()) {
                //pego um funcionario especifico e calculo os bonus pra esse funcionario na data selecionada
                var funcSalarioBonus = calculaSalarioBonusParaFuncionario(func);
                //insiro na collection de salarioBonus
                SalarioBonusCollection.getInstancia().addSalarioBonus(funcSalarioBonus);
                nomes.add(func.getNome());
            }
            //imprime o log
            LoggerPresenter.getInstancia().imprimirLogCalculo(nomes);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(calcularSalarioView, e.getMessage(), "ERRO!", JOptionPane.OK_OPTION);
        }
    }

    public SalarioBonusModel calculaSalarioBonusParaFuncionario(FuncionarioModel func) {
        SalarioBonusModel salarioBonus = new SalarioBonusModel(func.getId(), LocalDate.parse(calcularSalarioView.getDataCalculoSalario().getText(), DateTimeFormatter.ofPattern("dd/MM/yyyy")), func.getSalario(), func.getBonus().equals("Generoso"));
        new CalculadoraBonusCOR().calculadora(salarioBonus);
        return salarioBonus;
    }

    @Override
    public void update(SalarioBonusCollection dadosCollection) {
        //ATUALIZA A TABELA
        if (!this.calcularSalarioView.getDataBusca().getText().replace("/", " ").isBlank()) {
            this.refinarBusca(this.calcularSalarioView.getDataBusca().getText());
        } else {
            this.listarTodosBonusSalario();
        }
    }

}
