/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Collection.FuncionarioCollection;
import Model.FuncionarioModel;
import Presenter.Observer.FuncionarioObserver;
import View.ManterFuncionario.BuscaFuncionarioView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Lucas Carvalho
 */
public class BuscaFuncionarioPresenter implements FuncionarioObserver {

    private BuscaFuncionarioView buscaFuncionarioView;

    public BuscaFuncionarioPresenter(JDesktopPane desktop) {
        this.buscaFuncionarioView = new BuscaFuncionarioView();
        desktop.add(this.buscaFuncionarioView);
        this.buscaFuncionarioView.setVisible(true);
        //adiciona na lista de observers
        FuncionarioCollection.getInstancia().add(this);
        //exibir todos os resultados assim que a tela é criada
        exibirTodos();
        this.buscaFuncionarioView.getBotaoBuscarFuncionarioFiltro().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA REALIZAR UMA BUSCA REFINADA
                buscaRefinada(buscaFuncionarioView.getTextoFuncionarioFiltro().getText());
            }
        });
        this.buscaFuncionarioView.getBotaoNovoFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA INCLUIR NOVO FUNCIONARIO
                new ManterFuncionarioPresenter(null, desktop);
            }
        });
        this.buscaFuncionarioView.getBotaoVerBonusFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA VISUALIZAR A MODAL DE HISTORICO DE BONUS DO FUNCIONARIO
                var func = getSelectedFuncionario(); //pega o funcionário selecionado
                if(func != null){
                    new BonusFuncionarioModalPresenter(func);
                }else{
                    JOptionPane.showMessageDialog(buscaFuncionarioView, "Nenhum funcionario selecionado.", "ERRO!",JOptionPane.OK_OPTION);
                }
            }
        });
        this.buscaFuncionarioView.getBotaoVisualizarFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA ABRIR A TELA DE VISUALIZAÇÃO DO FUNCIONARIO SELECIONADO
                verificaLinhaSelecionada(); //exceção de nenhum funcionário selecionado
                var func = getSelectedFuncionario(); //pega o funcionário selecionado
                new ManterFuncionarioPresenter(func, desktop);
            }
        });
        this.buscaFuncionarioView.getBotaoFechar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA FECHAR A TELA DE BUSCA
                buscaFuncionarioView.dispose();
            }
        });
    }

    @Override
    public void update(FuncionarioCollection dadosCollection) {
        //ATUALIZA A TABELA
        if (!this.buscaFuncionarioView.getTextoFuncionarioFiltro().getText().isBlank()) {
            this.buscaRefinada(this.buscaFuncionarioView.getTextoFuncionarioFiltro().getText());
        } else {
            this.exibirTodos();
        }
    }

    public void exibirTodos() {
        this.exibeTabela(FuncionarioCollection.getInstancia().getFuncionarioCollection());
    }

    public void buscaRefinada(String busca) {
        //Realiza a busca na Collection com a refinação
        List<FuncionarioModel> listaBusca = FuncionarioCollection.getInstancia().getBuscaRefinada(busca);
        //exibir a busca refinada na tela
        this.exibeTabela(listaBusca);
    }

    public void exibeTabela(List<FuncionarioModel> listaFuncionario) {
        //Pega a referencia da tabela na view
        DefaultTableModel model = (DefaultTableModel) this.buscaFuncionarioView.getTabelaFuncionario().getModel();
        //limpa a tabela
        model.setRowCount(0);
        //preenche a tabela
        for (var func : listaFuncionario) {
            String[] dados = {String.valueOf(func.getId()), func.getNome(), String.valueOf(func.getIdade()), func.getCargo(), String.valueOf(func.getBonus()), String.valueOf(func.getSalario())};
            model.addRow(dados);
        }
    }

    public FuncionarioModel getSelectedFuncionario() {
        //Pega a referencia da tabela na view
        DefaultTableModel model = (DefaultTableModel) this.buscaFuncionarioView.getTabelaFuncionario().getModel();
        //pega a linha selecionada
        var linhaSelecionada = this.buscaFuncionarioView.getTabelaFuncionario().getSelectedRow();
        if (linhaSelecionada != -1) {
            //pega o ID do funcionario que ta na linha selecionada
            var id = Integer.parseInt(model.getValueAt(linhaSelecionada, 0).toString());
            //pega o funcionario na collection que possui aquele ID da tabela
            var func = FuncionarioCollection.getInstancia().getFuncionarioModelPorID(id);
            //retorna esse funcionario
            return func;
        }
        return null;
    }

    public void verificaLinhaSelecionada() {
        var linhaSelecionada = this.buscaFuncionarioView.getTabelaFuncionario().getSelectedRow();
        try {
            if (linhaSelecionada == -1) {
                throw new Exception("Nenhum funcionário selecionado, abrindo tela de novo funcionário.");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(buscaFuncionarioView, e.getMessage(), "WARNING!", JOptionPane.OK_OPTION);
        }
    }
}
