/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Adapter;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lucas Carvalho
 */
public class JSONAdapter extends AbstractAdapter {

    private final Path currentRelativePath = Paths.get("");
    private final String path = currentRelativePath.toAbsolutePath().toString();

    @Override
    public void imprimirLogCRUD(String nome, String operacao) {
        String resultadoConversaoJSON = "{" + "\"Funcionario\"" + ": \"" + nome + "\", "+ "\"Operacao\"" + ": " + operacao + "}";
        imprimir(resultadoConversaoJSON);
    }

    @Override
    public void imprimirLogConsulta(String nome) {
        String resultadoConversaoJSON = "{" + "\"Funcionario\"" + ": \"" + nome + "\", "+ "\"Operacao\"" + ": " + "\"Consulta_Bonus\"" + "}";
        imprimir(resultadoConversaoJSON);
    }

    @Override
    public void imprimirLogCalculo(List<String> nomes) {
        String resultadoConversaoJSON = "{" + "\"Calculo_Salario\"" + ": " + "[";
        for (String nome : nomes) {
            resultadoConversaoJSON = resultadoConversaoJSON + "{" + "\"Funcionario\"" + ": \"" + nome + "\"}";
        }
        resultadoConversaoJSON = resultadoConversaoJSON + "]" + "}";
        imprimir(resultadoConversaoJSON);
    }

    @Override
    public void imprimirLogFalha(String descricao) {
        String resultadoConversaoJSON = "{" + "\"Falha\"" + ": \"" + descricao + "\"}";
        imprimir(resultadoConversaoJSON);
    }

    @Override
    public void imprimir(String resultadoConversaoJSON) {
        try {
            FileWriter file = new FileWriter(path + "/src/main/java/log.xml", true);
            file.write(resultadoConversaoJSON);
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(XMLAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
