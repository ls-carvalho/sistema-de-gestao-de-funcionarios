/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Adapter;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lucas Carvalho
 */
public class TXTAdapter extends AbstractAdapter {

    private final Path currentRelativePath = Paths.get("");
    private final String path = currentRelativePath.toAbsolutePath().toString();

    @Override
    public void imprimirLogCRUD(String nome, String operacao) {
        String resultadoConversaoTXT = "Funcionario " + nome + " " + operacao + ".\n";
        imprimir(resultadoConversaoTXT);
    }

    @Override
    public void imprimirLogConsulta(String nome) {
        String resultadoConversaoTXT = "Bonus consultado para o funcionario " + nome + ".\n";
        imprimir(resultadoConversaoTXT);
    }

    @Override
    public void imprimirLogCalculo(List<String> nomes) {
        String resultadoConversaoTXT = "Salario calculado para o(s) funcionario(s): [\n";
        for (String nome : nomes) {
            resultadoConversaoTXT = resultadoConversaoTXT + nome + "\n";
        }
        resultadoConversaoTXT = resultadoConversaoTXT + "]\n";
        imprimir(resultadoConversaoTXT);
    }

    @Override
    public void imprimirLogFalha(String descricao) {
        String resultadoConversaoTXT = "Falha ao realizar a operacao: " + descricao + ".\n";
        imprimir(resultadoConversaoTXT);
        
    }
    
    @Override
    public void imprimir(String resultadoConversaoTXT){
        try {
            FileWriter file = new FileWriter(path + "/src/main/java/log.txt", true);
            file.write(resultadoConversaoTXT);
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(XMLAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
