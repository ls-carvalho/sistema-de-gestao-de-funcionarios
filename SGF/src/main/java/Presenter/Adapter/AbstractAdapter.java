/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Adapter;

import java.util.List;

/**
 *
 * @author Lucas Carvalho
 */
public abstract class AbstractAdapter {

    public abstract void imprimirLogCRUD(String nome, String operacao);

    public abstract void imprimirLogConsulta(String nome);

    public abstract void imprimirLogCalculo(List<String> nomes);

    public abstract void imprimirLogFalha(String descricao);
   
    public abstract void imprimir(String resultadoConversao);
}
