/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Adapter;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lucas Carvalho
 */
public class XMLAdapter extends AbstractAdapter {

    private final Path currentRelativePath = Paths.get("");
    private final String path = currentRelativePath.toAbsolutePath().toString();

    @Override
    public void imprimirLogCRUD(String nome, String operacao) {
        String resultadoConversaoXML = "<log>" + "<funcionario>" + nome + "<\\funcionario>" + "<operacao>" + operacao + "<\\operacao>" + "<\\log>";
        imprimir(resultadoConversaoXML);
    }

    @Override
    public void imprimirLogConsulta(String nome) {
        String resultadoConversaoXML = "<log>" + "<funcionario>" + nome + "<\\funcionario>" + "<operacao>" + "Consulta_Bonus" + "<\\operacao>" + "<\\log>";
        imprimir(resultadoConversaoXML);
    }

    @Override
    public void imprimirLogCalculo(List<String> nomes) {
        String resultadoConversaoXML = "<log>" + "<calculosalario>";
        for (String nome : nomes) {
            resultadoConversaoXML = resultadoConversaoXML + "<funcionario>" + nome + "<\\funcionario>";
        }
        resultadoConversaoXML = resultadoConversaoXML + "<\\calculosalario>" + "<\\log>";
        imprimir(resultadoConversaoXML);
    }

    @Override
    public void imprimirLogFalha(String descricao) {
        String resultadoConversaoXML = "<log>" + "<falha>" + descricao + "<\\falha>" + "<\\log>";
        imprimir(resultadoConversaoXML);
    }

    @Override
    public void imprimir(String resultadoConversaoXML) {
        try {
            FileWriter file = new FileWriter(path + "/src/main/java/log.xml", true);
            file.write(resultadoConversaoXML);
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(XMLAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
