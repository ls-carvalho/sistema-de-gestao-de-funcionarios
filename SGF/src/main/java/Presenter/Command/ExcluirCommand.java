/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Command;

import Collection.FuncionarioCollection;
import Presenter.ManterFuncionarioPresenter;

/**
 *
 * @author Lucas Carvalho
 */
public class ExcluirCommand extends ManterFuncionarioCommand {

    private FuncionarioCollection collection;

    public ExcluirCommand() {
        collection = FuncionarioCollection.getInstancia();
    }

    @Override
    public void executar(ManterFuncionarioPresenter manterFuncionarioPresenter) throws Exception {
        //codigo de excluir o registro
        try {
            collection.removeFuncionarioModelPorID(manterFuncionarioPresenter.getFuncionarioModel().getId());
        } catch (Exception e) {
            throw e;
        }
    }
}
