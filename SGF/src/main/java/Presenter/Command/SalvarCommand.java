/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Command;

import Collection.FuncionarioCollection;
import Model.FuncionarioModel;
import Presenter.ManterFuncionarioPresenter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Lucas Carvalho
 */
public class SalvarCommand extends ManterFuncionarioCommand {

    private FuncionarioCollection collection;

    public SalvarCommand() {
        this.collection = FuncionarioCollection.getInstancia();
    }

    @Override
    public void executar(ManterFuncionarioPresenter manterFuncionarioPresenter) throws Exception {
        //codigo de salvar o registro
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            var nome = manterFuncionarioPresenter.getManterFuncionarioView().getNomeFuncionario().getText();
            var cargo = String.valueOf(manterFuncionarioPresenter.getManterFuncionarioView().getCargoFuncionario().getSelectedItem());
            var idade = Integer.parseInt(manterFuncionarioPresenter.getManterFuncionarioView().getIdadeFuncionario().getText());
            var bonus = manterFuncionarioPresenter.getManterFuncionarioView().getBonusFuncionario().getSelectedItem().toString();
            var salario = Double.parseDouble(manterFuncionarioPresenter.getManterFuncionarioView().getSalarioFuncionario().getText().replace(",", "."));
            var faltas = Integer.parseInt(manterFuncionarioPresenter.getManterFuncionarioView().getFaltasFuncionario().getText());
            var data = LocalDate.parse(manterFuncionarioPresenter.getManterFuncionarioView().getAdmissaoFuncionario().getText(), formatter);
            var fmes = manterFuncionarioPresenter.getManterFuncionarioView().getFuncionarioDoMes().isSelected();
            if (manterFuncionarioPresenter.getFuncionarioModel() == null) {
                var id = collection.getNextIdFuncionario();
                FuncionarioModel novofuncionario = new FuncionarioModel(nome, cargo, idade, bonus, salario, faltas, data, fmes, id);
                manterFuncionarioPresenter.setFuncionarioModel(novofuncionario);
                collection.addFuncionario(novofuncionario);
            } else {
                if (collection.getFuncionarioModelPorID(manterFuncionarioPresenter.getFuncionarioModel().getId()) == null) {
                    //verificar ele na model
                    new Exception("Esse registro não existe.");
                }
                //substituir os campos
                manterFuncionarioPresenter.getFuncionarioModel().setNome(nome);
                manterFuncionarioPresenter.getFuncionarioModel().setCargo(cargo);
                manterFuncionarioPresenter.getFuncionarioModel().setIdade(idade);
                manterFuncionarioPresenter.getFuncionarioModel().setBonus(bonus);
                manterFuncionarioPresenter.getFuncionarioModel().setSalario(salario);
                manterFuncionarioPresenter.getFuncionarioModel().setFalta(faltas);
                manterFuncionarioPresenter.getFuncionarioModel().setAdmissao(data);
                manterFuncionarioPresenter.getFuncionarioModel().setFuncionarioDoMes(fmes);
                //alterar ele no banco
                collection.setFuncionarioModelPorID(manterFuncionarioPresenter.getFuncionarioModel(), manterFuncionarioPresenter.getFuncionarioModel().getId());
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
