/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Collection.FuncionarioCollection;
import Model.FuncionarioModel;
import Presenter.Observer.FuncionarioObserver;
import Presenter.State.InclusaoState;
import Presenter.State.ManterFuncionarioAbstractState;
import Presenter.State.VisualizacaoState;
import View.ManterFuncionario.ManterFuncionarioView;
import java.awt.event.ActionListener;
import javax.swing.JDesktopPane;

/**
 *
 * @author Lucas Carvalho
 */
public class ManterFuncionarioPresenter implements FuncionarioObserver {

    private ManterFuncionarioView manterFuncionarioView;
    private ManterFuncionarioAbstractState estado;
    private FuncionarioModel funcionarioModel;

    public ManterFuncionarioPresenter(FuncionarioModel funcionarioModel, JDesktopPane desktop) {
        this.manterFuncionarioView = new ManterFuncionarioView();
        desktop.add(this.manterFuncionarioView);
        this.manterFuncionarioView.setVisible(true);
        //adiciona na lista de observers
        FuncionarioCollection.getInstancia().add(this);
        //abriu pra mostrar um funcionario, ou pra incluir registro?
        if (funcionarioModel != null) {
            this.funcionarioModel = funcionarioModel;
            this.estado = new VisualizacaoState(this);
        } else {
            this.estado = new InclusaoState(this);
        }
    }

    public void salvar() {
        estado.salvar();
    }

    public void editar() {
        estado.editar();
    }

    public void excluir() {
        estado.excluir();
    }

    public void cancelar() {
        estado.cancelar();
    }

    public void setEstado(ManterFuncionarioAbstractState estado) {
        this.estado = estado;
    }

    public ManterFuncionarioView getManterFuncionarioView() {
        return manterFuncionarioView;
    }

    public FuncionarioModel getFuncionarioModel() {
        return funcionarioModel;
    }

    public void setFuncionarioModel(FuncionarioModel funcionarioModel) {
        this.funcionarioModel = funcionarioModel;
    }

    public void removeListeners() {
        if (this.getManterFuncionarioView().getBotaoSalvarConfimarFuncionario().getActionListeners() != null) {
            for (ActionListener actionListener : this.getManterFuncionarioView().getBotaoSalvarConfimarFuncionario().getActionListeners()) {
                this.getManterFuncionarioView().getBotaoSalvarConfimarFuncionario().removeActionListener(actionListener);
            }
        }
        if (this.getManterFuncionarioView().getBotaoEditarFuncionario().getActionListeners() != null) {
            for (ActionListener actionListener : this.getManterFuncionarioView().getBotaoEditarFuncionario().getActionListeners()) {
                this.getManterFuncionarioView().getBotaoEditarFuncionario().removeActionListener(actionListener);
            }
        }
        if (this.getManterFuncionarioView().getBotaoRemoverFuncionario().getActionListeners() != null) {
            for (ActionListener actionListener : this.getManterFuncionarioView().getBotaoRemoverFuncionario().getActionListeners()) {
                this.getManterFuncionarioView().getBotaoRemoverFuncionario().removeActionListener(actionListener);
            }
        }
        if (this.getManterFuncionarioView().getBotaoCancelar().getActionListeners() != null) {
            for (ActionListener actionListener : this.getManterFuncionarioView().getBotaoCancelar().getActionListeners()) {
                this.getManterFuncionarioView().getBotaoCancelar().removeActionListener(actionListener);
            }
        }
    }

    public void validacaoInclusao() throws Exception {
        try {
            if (this.getManterFuncionarioView().getNomeFuncionario().getText().isBlank()) {
                throw new Exception("Campo nome não pode ficar vazio!");
            }
            if (this.getManterFuncionarioView().getIdadeFuncionario().getText().isBlank()) {
                throw new Exception("Campo idade não pode ficar vazio!");
            }
            if (this.getManterFuncionarioView().getSalarioFuncionario().getText().isBlank()) {
                throw new Exception("Campo salario não pode ficar vazio!");
            }
            if (this.getManterFuncionarioView().getFaltasFuncionario().getText().isBlank()) {
                throw new Exception("Campo faltas não pode ficar vazio!");
            }
            if (this.getManterFuncionarioView().getAdmissaoFuncionario().getText().isBlank()) {
                throw new Exception("Campo admissão não pode ficar vazio!");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void limparCampos() {
        this.getManterFuncionarioView().getCargoFuncionario().setSelectedIndex(0);
        this.getManterFuncionarioView().getNomeFuncionario().setText("");
        this.getManterFuncionarioView().getIdadeFuncionario().setText("");
        this.getManterFuncionarioView().getBonusFuncionario().setSelectedIndex(0);
        this.getManterFuncionarioView().getSalarioFuncionario().setText("");
        this.getManterFuncionarioView().getFaltasFuncionario().setText("");
        this.getManterFuncionarioView().getFuncionarioDoMes().setSelected(false);
        this.getManterFuncionarioView().getAdmissaoFuncionario().setText("");
    }

    @Override
    public void update(FuncionarioCollection dadosCollection) {
        if (this.estado instanceof VisualizacaoState) {
            //Verifica um erro de MDI + Observer + State (Remoção)
            if (dadosCollection.getFuncionarioModelPorID(this.funcionarioModel.getId()) == null) {
                this.getManterFuncionarioView().dispose();
                return;
            }
            //Procura o elemento na collection com o ID respectivo e insere em this.funcionarioModel
            this.setFuncionarioModel(dadosCollection.getFuncionarioModelPorID(this.funcionarioModel.getId()));
            //atualizar os campos da view 
            this.getManterFuncionarioView().getCargoFuncionario().setSelectedItem(this.getFuncionarioModel().getCargo());
            this.getManterFuncionarioView().getNomeFuncionario().setText(this.getFuncionarioModel().getNome());
            this.getManterFuncionarioView().getIdadeFuncionario().setText(String.valueOf(this.getFuncionarioModel().getIdade()));
            this.getManterFuncionarioView().getBonusFuncionario().setSelectedItem(this.getFuncionarioModel().getBonus());
            this.getManterFuncionarioView().getSalarioFuncionario().setText(String.valueOf(this.getFuncionarioModel().getSalario()));
            this.getManterFuncionarioView().getFaltasFuncionario().setText(String.valueOf(this.getFuncionarioModel().getFalta()));
            this.getManterFuncionarioView().getFuncionarioDoMes().setSelected(this.getFuncionarioModel().isFuncionarioDoMes());
            this.getManterFuncionarioView().getAdmissaoFuncionario().setText(this.getFuncionarioModel().getAdmissao().toString());
        }
    }
}
