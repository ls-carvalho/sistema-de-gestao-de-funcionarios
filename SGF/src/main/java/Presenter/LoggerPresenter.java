/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Presenter.Adapter.AbstractAdapter;
import Presenter.Adapter.JSONAdapter;
import Presenter.Adapter.TXTAdapter;
import Presenter.Adapter.XMLAdapter;
import java.util.List;

/**
 *
 * @author Lucas Carvalho
 */
public class LoggerPresenter {

    private static LoggerPresenter instancia;
    private AbstractAdapter adapter;

    private LoggerPresenter() {
        this.adapter = new XMLAdapter();
    }

    public static LoggerPresenter getInstancia() {
        if (instancia == null) {
            instancia = new LoggerPresenter();
        }
        return instancia;
    }

    public void imprimirLogCRUD(String nome, String operacao) {
        adapter.imprimirLogCRUD(nome, operacao);
    }

    public void imprimirLogConsulta(String nome) {
        adapter.imprimirLogConsulta(nome);
    }

    public void imprimirLogCalculo(List<String> nomes) {
        adapter.imprimirLogCalculo(nomes);
    }

    public void imprimirLogFalha(String descricao) {
        adapter.imprimirLogFalha(descricao);
    }

    public void setAdapter(String tipo) {
        AbstractAdapter aux = null;
        switch (tipo) {
            case "JSON":
                aux = new JSONAdapter();
                break;
            case "XML":
                aux = new XMLAdapter();
                break;
            case "TXT":
                aux = new TXTAdapter();
                break;
            default:
                break;
        }
        this.adapter = aux;
    }
}
