/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Observer;

import Collection.FuncionarioCollection;

/**
 *
 * @author Lucas Carvalho
 */
public interface FuncionarioObserver {

    public void update(FuncionarioCollection dadosCollection);

}
