/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Observer;

import Collection.FuncionarioCollection;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucas Carvalho
 */
public abstract class FuncionarioSubject {

    private List<FuncionarioObserver> observers = new ArrayList();

    public void add(FuncionarioObserver observer) {
        this.observers.add(observer);
    }

    public void notifyObserver() {
        for (var observer : this.observers) {
            observer.update(FuncionarioCollection.getInstancia());
        }
    }
}
