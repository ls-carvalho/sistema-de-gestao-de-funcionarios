/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Observer;

import Collection.SalarioBonusCollection;

/**
 *
 * @author Lucas Carvalho
 */
public interface SalarioBonusObserver {
    
    public void update(SalarioBonusCollection dadosCollection);

}
