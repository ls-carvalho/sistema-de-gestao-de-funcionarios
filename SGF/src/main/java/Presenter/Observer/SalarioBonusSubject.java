/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Observer;

import Collection.SalarioBonusCollection;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucas Carvalho
 */
public abstract class SalarioBonusSubject {

    private List<SalarioBonusObserver> observers = new ArrayList();

    public void add(SalarioBonusObserver observer) {
        this.observers.add(observer);
    }

    public void notifyObserver() {
        for (var observer : this.observers) {
            observer.update(SalarioBonusCollection.getInstancia());
        }
    }
}
