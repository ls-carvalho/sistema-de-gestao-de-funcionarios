/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.State;

import Presenter.Command.SalvarCommand;
import Presenter.LoggerPresenter;
import Presenter.ManterFuncionarioPresenter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Lucas Carvalho
 */
public class EdicaoState extends ManterFuncionarioAbstractState {

    public EdicaoState(ManterFuncionarioPresenter manterFuncionarioPresenter) {
        super(manterFuncionarioPresenter);
        //faz um removeActionListener() em cada listener
        super.getManterFuncionarioPresenter().removeListeners();
        //recria os listeners pra essa tela com addActionListener()
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoSalvarConfimarFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA SALVAR EDIÇÃO
                salvar();
            }
        });
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoCancelar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA CANCELAR(SAIR) INCLUSAO
                cancelar();
            }
        });
        //"Arruma" a view, dando enable em campos e limpando quando necessário.
        //preenche os campos
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getCargoFuncionario().setSelectedItem(super.getManterFuncionarioPresenter().getFuncionarioModel().getCargo());
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getNomeFuncionario().setText(super.getManterFuncionarioPresenter().getFuncionarioModel().getNome());
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getIdadeFuncionario().setText(String.valueOf(super.getManterFuncionarioPresenter().getFuncionarioModel().getIdade()));
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBonusFuncionario().setSelectedItem(super.getManterFuncionarioPresenter().getFuncionarioModel().getBonus());
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getSalarioFuncionario().setText(String.valueOf(super.getManterFuncionarioPresenter().getFuncionarioModel().getSalario()));
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getFaltasFuncionario().setText(String.valueOf(super.getManterFuncionarioPresenter().getFuncionarioModel().getFalta()));
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getFuncionarioDoMes().setSelected(super.getManterFuncionarioPresenter().getFuncionarioModel().isFuncionarioDoMes());
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getAdmissaoFuncionario().setText(super.getManterFuncionarioPresenter().getFuncionarioModel().getAdmissao().toString());
        //enable nos campos
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getCargoFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getNomeFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getIdadeFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBonusFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getSalarioFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getFaltasFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getFuncionarioDoMes().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getAdmissaoFuncionario().setEnabled(true);
        //enable/disable nos botoes
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoSalvarConfimarFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoRemoverFuncionario().setEnabled(false);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoEditarFuncionario().setEnabled(false);
    }

    @Override
    public void salvar() {
        try {
            //salva o registro
            new SalvarCommand().executar(super.getManterFuncionarioPresenter());
            //log de edição
            LoggerPresenter.getInstancia().imprimirLogCRUD(super.getManterFuncionarioPresenter().getFuncionarioModel().getNome(), "alterado");
            //mudar de estado
            super.getManterFuncionarioPresenter().setEstado(new VisualizacaoState(super.getManterFuncionarioPresenter()));
        } catch (Exception e) {
            LoggerPresenter.getInstancia().imprimirLogFalha("alteracao" + "(" + e.getMessage() + ")");
            JOptionPane.showMessageDialog(getManterFuncionarioPresenter().getManterFuncionarioView(), e.getMessage(), "ERRO!", JOptionPane.OK_OPTION);
        }
    }

    @Override
    public void cancelar() {
        //mudar de estado
        super.getManterFuncionarioPresenter().setEstado(new VisualizacaoState(super.getManterFuncionarioPresenter()));
    }
}
