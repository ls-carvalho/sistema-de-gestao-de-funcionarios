/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.State;

import Presenter.ManterFuncionarioPresenter;

/**
 *
 * @author Lucas Carvalho
 */
public abstract class ManterFuncionarioAbstractState {

    private ManterFuncionarioPresenter manterFuncionarioPresenter;

    public ManterFuncionarioAbstractState(ManterFuncionarioPresenter manterFuncionarioPresenter) {
        this.manterFuncionarioPresenter = manterFuncionarioPresenter;
    }

    public void salvar() {
        throw new RuntimeException("Não é possivel usar esse metodo neste estado.");
    }

    public void editar() {
        throw new RuntimeException("Não é possivel usar esse metodo neste estado.");
    }

    public void excluir() {
        throw new RuntimeException("Não é possivel usar esse metodo neste estado.");
    }

    public void cancelar() {
        this.manterFuncionarioPresenter.getManterFuncionarioView().dispose();
    }

    public ManterFuncionarioPresenter getManterFuncionarioPresenter() {
        return manterFuncionarioPresenter;
    }
    
}
