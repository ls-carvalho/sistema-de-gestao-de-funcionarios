/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.State;

import Presenter.Command.ExcluirCommand;
import Presenter.LoggerPresenter;
import Presenter.ManterFuncionarioPresenter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;
import javax.swing.JOptionPane;

/**
 *
 * @author Lucas Carvalho
 */
public class VisualizacaoState extends ManterFuncionarioAbstractState {

    public VisualizacaoState(ManterFuncionarioPresenter manterFuncionarioPresenter) {
        super(manterFuncionarioPresenter);
        //faz um removeActionListener() em cada listener
        super.getManterFuncionarioPresenter().removeListeners();
        //recria os listeners pra essa tela com addActionListener()
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoEditarFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA SALVAR EDIÇÃO
                editar();
            }
        });
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoRemoverFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA EXCLUIR REGISTRO
                excluir();
            }
        });
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoCancelar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA CANCELAR(SAIR) VISUALIZAÇÃO
                cancelar();
            }
        });
        //"Arruma" a view, dando enable em campos e limpando quando necessário.
        //preenche os campos
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getCargoFuncionario().setSelectedItem(super.getManterFuncionarioPresenter().getFuncionarioModel().getCargo());
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getNomeFuncionario().setText(super.getManterFuncionarioPresenter().getFuncionarioModel().getNome());
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getIdadeFuncionario().setText(String.valueOf(super.getManterFuncionarioPresenter().getFuncionarioModel().getIdade()));
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBonusFuncionario().setSelectedItem(super.getManterFuncionarioPresenter().getFuncionarioModel().getBonus());
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getSalarioFuncionario().setText(String.valueOf(super.getManterFuncionarioPresenter().getFuncionarioModel().getSalario()));
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getFaltasFuncionario().setText(String.valueOf(super.getManterFuncionarioPresenter().getFuncionarioModel().getFalta()));
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getFuncionarioDoMes().setSelected(super.getManterFuncionarioPresenter().getFuncionarioModel().isFuncionarioDoMes());
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getAdmissaoFuncionario().setText(super.getManterFuncionarioPresenter().getFuncionarioModel().getAdmissao().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        //disable nos campos
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getCargoFuncionario().setEnabled(false);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getNomeFuncionario().setEnabled(false);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getIdadeFuncionario().setEnabled(false);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBonusFuncionario().setEnabled(false);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getSalarioFuncionario().setEnabled(false);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getFaltasFuncionario().setEnabled(false);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getFuncionarioDoMes().setEnabled(false);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getAdmissaoFuncionario().setEnabled(false);
        //enable/disable nos botoes
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoSalvarConfimarFuncionario().setEnabled(false);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoRemoverFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoEditarFuncionario().setEnabled(true);
    }

    @Override
    public void editar() {
        //mudar de estado
        super.getManterFuncionarioPresenter().setEstado(new EdicaoState(super.getManterFuncionarioPresenter()));
    }

    @Override
    public void excluir() {
        int excluir = JOptionPane.showConfirmDialog(super.getManterFuncionarioPresenter().getManterFuncionarioView(), "Tem certeza que deseja excluir esse funcionário?", "ATENÇÃO", JOptionPane.OK_CANCEL_OPTION);
        if (excluir == 0) {
            try {
                //exclui o registro
                new ExcluirCommand().executar(super.getManterFuncionarioPresenter());
                //log da operação
                LoggerPresenter.getInstancia().imprimirLogCRUD(super.getManterFuncionarioPresenter().getFuncionarioModel().getNome(), "removido");                
                //fechar janela
                super.getManterFuncionarioPresenter().getManterFuncionarioView().dispose();
                JOptionPane.showMessageDialog(getManterFuncionarioPresenter().getManterFuncionarioView(), "Exclusão bem sucedida!", "SUCESSO!", JOptionPane.OK_OPTION);
            } catch (Exception e) {
                LoggerPresenter.getInstancia().imprimirLogFalha("remocao" + "(" + e.getMessage() + ")");
                JOptionPane.showMessageDialog(getManterFuncionarioPresenter().getManterFuncionarioView(), e.getMessage(), "ERRO!", JOptionPane.OK_OPTION);
            }
        } else {
            //"mudar" de estado: mesmo que o estado não mude, eu preciso garantir que o construtor refaça seu trabalho
            super.getManterFuncionarioPresenter().setEstado(new VisualizacaoState(super.getManterFuncionarioPresenter()));
        }
    }
}
