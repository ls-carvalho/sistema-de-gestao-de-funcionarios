/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.State;

import Presenter.Command.SalvarCommand;
import Presenter.LoggerPresenter;
import Presenter.ManterFuncionarioPresenter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Lucas Carvalho
 */ 
public class InclusaoState extends ManterFuncionarioAbstractState {
   

    public InclusaoState(ManterFuncionarioPresenter manterFuncionarioPresenter) {
        super(manterFuncionarioPresenter);
        //faz um removeActionListener() em cada listener
        super.getManterFuncionarioPresenter().removeListeners();
        //recria os listeners pra essa tela com addActionListener()
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoSalvarConfimarFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA SALVAR INCLUSAO
                salvar();
            }
        });
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoCancelar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA CANCELAR(SAIR) INCLUSAO
                cancelar();
            }
        });
        //"Arruma" a view, dando enable em campos e limpando quando necessário.
        //limpar os campos
        super.getManterFuncionarioPresenter().limparCampos();
        //enable nos campos
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getCargoFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getNomeFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getIdadeFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBonusFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getSalarioFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getFaltasFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getFuncionarioDoMes().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getAdmissaoFuncionario().setEnabled(true);
        //enable ou disable nos botoes
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoSalvarConfimarFuncionario().setEnabled(true);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoRemoverFuncionario().setEnabled(false);
        super.getManterFuncionarioPresenter().getManterFuncionarioView().getBotaoEditarFuncionario().setEnabled(false);
    }

    @Override
    public void salvar() {
        try {
            //validar campos
            super.getManterFuncionarioPresenter().validacaoInclusao();
            //salvar o registro
            new SalvarCommand().executar(super.getManterFuncionarioPresenter());
            //impressão de log
            LoggerPresenter.getInstancia().imprimirLogCRUD(super.getManterFuncionarioPresenter().getFuncionarioModel().getNome(), "adicionado");
            //mudar de estado
            getManterFuncionarioPresenter().setEstado(new VisualizacaoState(getManterFuncionarioPresenter()));
        } catch (Exception e) {
            LoggerPresenter.getInstancia().imprimirLogFalha("adicao" + "(" + e.getMessage() + ")");
            JOptionPane.showMessageDialog(getManterFuncionarioPresenter().getManterFuncionarioView(), e.getMessage(), "ERRO!", JOptionPane.OK_OPTION);
        }
    }

}
