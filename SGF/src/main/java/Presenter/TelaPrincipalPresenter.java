/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Collection.FuncionarioCollection;
import Presenter.Observer.FuncionarioObserver;
import View.PrincipalView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

/**
 *
 * @author Lucas Carvalho
 */
public class TelaPrincipalPresenter implements FuncionarioObserver {

    private PrincipalView principalView;

    public TelaPrincipalPresenter() {
        this.principalView = new PrincipalView();
        LoggerPresenter.getInstancia(); //inicia o singleton de logs assim que o programa abre
    }

    public void abrirTelaPrincipal() {
        this.principalView.setVisible(true);
        this.principalView.getBotaoManterFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA TELA DE MANTER FUNCIONÁRIO
                new ManterFuncionarioPresenter(null, principalView.getDesktop());
            }
        });
        this.principalView.getBotaoBuscarFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA TELA DE BUSCAR FUNCIONÁRIO
                new BuscaFuncionarioPresenter(principalView.getDesktop());
            }
        });
        this.principalView.getBotaoCalcularSalario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO PARA TELA DE CALCULAR SALARIOS
                new CalcularSalarioPresenter(principalView.getDesktop());
            }
        });
        this.principalView.getBotaoJSON().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO DE ALTERNAR LOG PARA .JSON
                trocarSelecaoLog(principalView.getBotaoJSON());
                LoggerPresenter.getInstancia().setAdapter("JSON");
            }
        });
        this.principalView.getBotaoTXT().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO DE ALTERNAR LOG PARA .TXT
                trocarSelecaoLog(principalView.getBotaoTXT());
                LoggerPresenter.getInstancia().setAdapter("TXT");
            }
        });
        this.principalView.getBotaoXML().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO DE ALTERNAR LOG PARA .XML
                trocarSelecaoLog(principalView.getBotaoXML());
                LoggerPresenter.getInstancia().setAdapter("XML");
            }
        });
    }

    private void trocarSelecaoLog(JMenuItem botaoSelecionado) {
        if (!".txt".equals(botaoSelecionado.getText())) {
            this.principalView.getBotaoTXT().setSelected(false);
        }
        if (!".xml".equals(botaoSelecionado.getText())) {
            this.principalView.getBotaoXML().setSelected(false);
        }
        if (!".json".equals(botaoSelecionado.getText())) {
            this.principalView.getBotaoJSON().setSelected(false);
        }
    }

    @Override
    public void update(FuncionarioCollection dadosCollection) {
        var numeroCadastrado = dadosCollection.getNumeroFuncionarioCadastrado();
        this.principalView.getNumeroFuncionariosCadastrados().setText(String.valueOf(numeroCadastrado));
    }
}
