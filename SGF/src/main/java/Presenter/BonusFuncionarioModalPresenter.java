/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Collection.SalarioBonusCollection;
import Model.FuncionarioModel;
import View.ManterFuncionario.BonusFuncionarioModalView;
import java.awt.Frame;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Lucas Carvalho
 */
public class BonusFuncionarioModalPresenter {

    private BonusFuncionarioModalView bonusFuncionarioModalView;

    public BonusFuncionarioModalPresenter(FuncionarioModel funcionario) {
        bonusFuncionarioModalView = new BonusFuncionarioModalView(new Frame(), true);
        bonusFuncionarioModalView.getNomeFuncionario().setText(funcionario.getNome());
        bonusFuncionarioModalView.getBonusGeneroso().setSelected(funcionario.getBonus().equals("Generoso"));
        bonusFuncionarioModalView.getCargoFuncionario().setText(funcionario.getCargo());
        bonusFuncionarioModalView.getDataConsulta().setText(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        DefaultTableModel model = (DefaultTableModel) this.bonusFuncionarioModalView.getTabelaBonusFuncionarioModal().getModel();
        for (var salarioBonusCalculado : SalarioBonusCollection.getInstancia().getSalarioCollectionPorFuncionarioID(funcionario.getId())) {
            for (var bonus : salarioBonusCalculado.getBonus()) {
                String[] dados = {bonus.getNome(), String.valueOf(bonus.getValor()), salarioBonusCalculado.getData().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))};
                model.addRow(dados);
            }
        }
        //cria o log da consulta
        LoggerPresenter.getInstancia().imprimirLogConsulta(funcionario.getNome());
        bonusFuncionarioModalView.setVisible(true);
    }
}
